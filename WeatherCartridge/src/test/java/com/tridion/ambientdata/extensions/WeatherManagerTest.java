package com.tridion.ambientdata.extensions;

import static org.testng.Assert.assertNotNull;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.tridion.ambientdata.extensions.bean.InputClaim;

public class WeatherManagerTest {
	WeatherManager manager = new WeatherManager();

	@BeforeMethod
	public void BeforeMethod() {
		InputClaim.setCity("Bengaluru");
		InputClaim.setCountry("India");
		manager.weatherLocationDetails();
	}

	@Test
	public void weatherLocationDetailsPropertyNotNull() {
		assertNotNull(manager.city);
		assertNotNull(manager.country);
		assertNotNull(manager.code);
		assertNotNull(manager.weatherLink);
		assertNotNull(manager.sunrise);
		assertNotNull(manager.sunset);
		assertNotNull(manager.weatherText);
		assertNotNull(manager.date);
		assertNotNull(manager.day);
		assertNotNull(manager.text);
	}

	@Test
	public void weatherLocationDetailsReturnNotNull() {
		assertNotNull(manager.weatherLocationDetails());
	}

	/*
	 * @Test public void weatherLocationDetailsTemperatureNotEqualZero(){ int
	 * low_Expected = 0; int high_Expected = 0; assertNotEquals(low_Expected,
	 * manager.low); assertNotEquals(high_Expected, manager.high); }
	 */
}
