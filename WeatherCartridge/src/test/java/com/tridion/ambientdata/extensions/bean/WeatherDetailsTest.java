package com.tridion.ambientdata.extensions.bean;

import static org.testng.Assert.assertNotNull;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.tridion.ambientdata.extensions.WeatherManagerTest;

public class WeatherDetailsTest {
	WeatherManagerTest manager = new WeatherManagerTest();

	@BeforeMethod
	public void BeforeMethod() {
		manager.BeforeMethod();
	}

	@Test
	public void getSunriseNotNull() {
		assertNotNull(WeatherDetails.getSunrise());
	}

	@Test
	public void getSunset() {
		assertNotNull(WeatherDetails.getSunset());
	}

	@Test
	public void getWeatherLink() {
		assertNotNull(WeatherDetails.getWeatherLink());
	}

	@Test
	public void getWeatherText() {
		assertNotNull(WeatherDetails.getWeatherText());
	}
}
