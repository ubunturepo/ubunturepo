package com.tridion.ambientdata.extensions.bean;

import static org.testng.Assert.assertNotNull;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.tridion.ambientdata.extensions.WeatherManagerTest;

public class InputClaimTest {

	WeatherManagerTest manager = new WeatherManagerTest();

	@BeforeMethod
	public void BeforeMethod() {
		manager.BeforeMethod();
	}

	@Test
	public void getCityNotNull() {
		assertNotNull(InputClaim.getCity());
	}

	@Test
	public void getCountryNotNull() {
		assertNotNull(InputClaim.getCountry());
	}

}
