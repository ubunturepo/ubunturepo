package com.tridion.ambientdata.extensions;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@Configuration
@ComponentScan("com.tridion.ambientdata.extensions")
public class WeatherConfig {

}
