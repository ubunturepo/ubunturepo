package com.tridion.ambientdata.extensions.bean;

public class WeatherDetailsJSON {
	private String code, date, day, text;
	int low, high;

	public WeatherDetailsJSON() {
		// TODO Auto-generated constructor stub
	}

	public WeatherDetailsJSON(String code, String date, String day,
			Integer low, Integer high, String text) {
		// TODO Auto-generated constructor stub
		this.code = code;
		this.date = date;
		this.day = day;
		this.low = low;
		this.high = high;
		this.text = text;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public double getLow() {
		return low;
	}

	public int getHigh() {
		return high;
	}

	public void setHigh(int high) {
		this.high = high;
	}

	public void setLow(int low) {
		this.low = low;
	}
}
