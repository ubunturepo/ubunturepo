package com.tridion.ambientdata.extensions;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;

import com.tridion.ambientdata.claimstore.ClaimStore;
import com.tridion.ambientdata.extensions.bean.InputClaim;
import com.tridion.ambientdata.extensions.bean.WeatherDetails;
import com.tridion.ambientdata.processing.AbstractClaimProcessor;

public class WeatherLocationProcessor extends AbstractClaimProcessor {
	private final Logger log = Logger.getLogger(WeatherLocationProcessor.class
			.getName());

	String country, city, ADFWeather;
	/*
	 * ApplicationContext context = new
	 * AnnotationConfigApplicationContext(WeatherConfig.class); WeatherManager
	 * manager = context.getBean(WeatherManager.class); Future<String> future =
	 * manager.weatherLocationDetails();
	 */
	WeatherManager manager = new WeatherManager();

	public void onRequestStart(ClaimStore claim) {
		log.debug("WeatherLocationProcessor.onRequestStart() called");
		try {
			country = (String) claim.get(new URI(
					"taf:claim:context:geolocation:locality"));
			city = (String) claim.get(new URI(
					"taf:claim:context:geolocation:city"));

			InputClaim.setCity(city);
			InputClaim.setCountry(country);

			/*
			 * try { ADFWeather = future.get(); } catch (Exception e) { // TODO:
			 * handle exception }
			 */
			ADFWeather = manager.weatherLocationDetails();

			claim.put(new URI("taf:claim:geolocation:weather:forecast"),
					ADFWeather);
			claim.put(new URI("taf:claim:geolocation:weather:current"),
					WeatherDetails.getWeatherText());
			claim.put(new URI("taf:claim:geolocation:weather:report"),
					WeatherDetails.getWeatherLink());
			claim.put(new URI("taf:claim:geolocation:weather:sunrise"),
					WeatherDetails.getSunrise());
			claim.put(new URI("taf:claim:geolocation:weather:sunset"),
					WeatherDetails.getSunset());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onRequestEnd(ClaimStore claim) {
		log.debug("WeatherLocationProcessor.onRequestEnd() called");
	}

	public void onSessionStart(ClaimStore claim) {
		log.debug("WeatherLocationProcessor.onSessionStart() called");
	}
}
