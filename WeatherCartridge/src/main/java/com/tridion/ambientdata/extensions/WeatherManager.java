package com.tridion.ambientdata.extensions;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.tridion.ambientdata.extensions.bean.InputClaim;
import com.tridion.ambientdata.extensions.bean.WeatherDetails;
import com.tridion.ambientdata.extensions.bean.WeatherDetailsJSON;
import com.tridion.ambientdata.extensions.utility.Utility;
import com.tridion.ambientdata.extensions.utility.UtilityImpl;

@Scope(value = "prototype")
@Component
public class WeatherManager {
	private final Logger log = Logger.getLogger(WeatherManager.class);
	Utility utility = new UtilityImpl();
	List<WeatherDetailsJSON> list = new ArrayList<WeatherDetailsJSON>();

	String city, country, weatherLink, sunrise, sunset, weatherText, code,
			date, day, text;
	int low, high;
	String ADFWeather;
	JSONObject jsonObject;

	@Async
	public String weatherLocationDetails() {
		log.debug("WeatherManager.weatherLocationDetails() called");
		city = InputClaim.getCity();
		country = InputClaim.getCountry();

		String JSONUrl = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text=%22"
				+ country + ",%20" + city + "%22)&format=json";
		try {
			jsonObject = new JSONObject(IOUtils.toString(new URL(JSONUrl),
					Charset.forName("UTF-8")));
			JSONObject query = jsonObject.getJSONObject("query");
			JSONObject result = query.getJSONObject("results");
			JSONObject channel = result.getJSONObject("channel");
			weatherLink = channel.getString("link");
			JSONObject astronomy = channel.getJSONObject("astronomy");
			sunrise = astronomy.getString("sunrise");
			sunset = astronomy.getString("sunset");
			JSONObject item = channel.getJSONObject("item");
			JSONObject condition = item.getJSONObject("condition");
			weatherText = condition.getString("text");
			JSONArray forecast = item.getJSONArray("forecast");

			WeatherDetails.setWeatherLink(weatherLink);
			WeatherDetails.setSunrise(sunrise);
			WeatherDetails.setSunset(sunset);
			WeatherDetails.setWeatherText(weatherText);

			for (int i = 0; i < forecast.length(); i++) {
				JSONObject first = forecast.getJSONObject(i);
				code = first.getString("code");
				date = first.getString("date");
				day = first.getString("day");
				low = utility
						.convertFahrenheittoCelsius(first.getString("low"));
				high = utility.convertFahrenheittoCelsius(first
						.getString("high"));
				text = first.getString("text");

				list.add(new WeatherDetailsJSON(code, date, day, low, high,
						text));
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

		ObjectMapper mapper = new ObjectMapper();

		try {
			ADFWeather = mapper.writeValueAsString(list);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ADFWeather;
	}
}
