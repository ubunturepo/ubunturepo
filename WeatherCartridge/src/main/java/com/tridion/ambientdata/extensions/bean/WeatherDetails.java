package com.tridion.ambientdata.extensions.bean;

public final class WeatherDetails {
	private static String weatherLink, sunrise, sunset, weatherText;

	public static String getWeatherLink() {
		return weatherLink;
	}

	public static void setWeatherLink(String _weatherLink) {
		weatherLink = _weatherLink;
	}

	public static String getSunrise() {
		return sunrise;
	}

	public static void setSunrise(String _sunrise) {
		sunrise = _sunrise;
	}

	public static String getSunset() {
		return sunset;
	}

	public static void setSunset(String _sunset) {
		sunset = _sunset;
	}

	public static String getWeatherText() {
		return weatherText;
	}

	public static void setWeatherText(String _weatherText) {
		weatherText = _weatherText;
	}

}
