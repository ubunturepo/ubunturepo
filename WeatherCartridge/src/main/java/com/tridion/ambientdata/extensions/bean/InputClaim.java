package com.tridion.ambientdata.extensions.bean;

public final class InputClaim {
	private static String city;
	private static String country;

	public static void setCity(String _city) {
		city = _city;
	}

	public static String getCity() {
		return city;
	}

	public static void setCountry(String _country) {
		country = _country;
	}

	public static String getCountry() {
		return country;
	}

}
