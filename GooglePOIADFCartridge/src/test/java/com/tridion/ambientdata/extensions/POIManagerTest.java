package com.tridion.ambientdata.extensions;

import static org.testng.Assert.assertNotNull;

import java.util.concurrent.Future;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class POIManagerTest {

	POIManager manager; 
	Future<String> string;

	@BeforeMethod
	public void beforeMethod() {
		manager = new POIManager();
		string = manager.GooglePOIDetails();
	}

	@Test
	public void GooglePOIDetailsReturnNotNull() {

		assertNotNull(string);
		
		assertNotNull(manager.list);
		assertNotNull(manager.jsonObject);
		assertNotNull(manager.ID);
		assertNotNull(manager.DisplayName);
		assertNotNull(manager.AddressLine1);
		assertNotNull(manager.Latitude);
		assertNotNull(manager.Longitude);
	}

}
