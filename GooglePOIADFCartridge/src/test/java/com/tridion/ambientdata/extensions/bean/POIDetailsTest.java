package com.tridion.ambientdata.extensions.bean;

import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.tridion.ambientdata.extensions.POIManager;

public class POIDetailsTest {

	POIDetails details;
	POIManager manager;
	public String testID;
	public String testDisplayName;
	public String testAddressLine1;
	public double testLatitude;
	public double testLongitude;
	
	@BeforeClass
	public void Before(){
		details = new POIDetails();
		manager = new POIManager();
		manager.GooglePOIDetails();
		testID = manager.ID;
		testDisplayName = manager.DisplayName;
		testAddressLine1 = manager.AddressLine1;
		testLatitude = manager.Latitude;
		testLongitude = manager.Longitude;
	}
	

	@Test
	public void getAddressLine1NotNull() {
		details.setAddressLine1(testAddressLine1);
		String Address = details.getAddressLine1();
		assertNotNull(Address);
	}

	@Test
	public void getDisplayNameNotNull() {
		details.setDisplayName(testDisplayName);
		String name = details.getDisplayName();
		assertNotNull(name);
	}

	@Test
	public void getIDNotNull() {
		details.setID(testID);
		String id = details.getID();
		assertNotNull(id);
	}

	@Test
	public void getLatitudeNotReturnZero() {
		double latitude_Expected = 0.0;
		details.setLatitude(testLatitude);
		double latitude = details.getLatitude();
		assertNotEquals(latitude_Expected, latitude);
	}

	@Test
	public void getLongitudeNotReturnZero() {
		double longitude_Expected = 0.0;
		details.setLongitude(testLongitude);
		double longitude = details.getLongitude();
		assertNotEquals(longitude_Expected, longitude);
	}
}
