package com.tridion.ambientdata.extensions.bean;

import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertNotNull;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.tridion.ambientdata.extensions.POIManager;

public class URLPropertiesTest {

	URLProperties properties;
	POIManager manager;
	String testPlace;
	String testLongitude;
	String testLatitude;
	String testRadius;
	String testKey;

	@BeforeMethod
	public void beforeMethod() {
		properties = new URLProperties();
		manager = new POIManager();
		manager.GooglePOIDetails();
		testPlace = manager.place;
		testLongitude = Double.toString(manager.longitude);
		testLatitude = Double.toString(manager.latitude);
		testRadius = Integer.toString(manager.radius);
		testKey = manager.key;
	}

	@Test
	public void getKey() {
		properties.setKey(testPlace);
		String place = properties.getKey();
		assertNotNull(place);
	}

	@Test
	public void getLatitude() {
		String latitude_expected = null;
		properties.setLatitude(testLatitude);
		String latitude = properties.getLatitude();
		assertNotEquals(latitude_expected, latitude);
	}

	@Test
	public void getLongitude() {
		String longitude_expected = null;
		properties.setLongitude(testLongitude);
		String longitude = properties.getLongitude();
		assertNotEquals(longitude_expected, longitude);
	}

	@Test
	public void getPlace() {
		properties.setPlace(testPlace);
		String place = properties.getPlace();
		assertNotNull(place);
	}

	@Test
	public void getRadius() {
		String radius_expected = null;
		properties.setRadius(testRadius);
		String radius = properties.getRadius();
		assertNotEquals(radius_expected, radius);
	}
}
