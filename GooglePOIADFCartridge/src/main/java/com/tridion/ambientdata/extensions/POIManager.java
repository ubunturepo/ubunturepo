package com.tridion.ambientdata.extensions;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import com.tridion.ambientdata.extensions.bean.POIDetails;
import com.tridion.ambientdata.extensions.bean.URLProperties;

@Component
public class POIManager {

	final Logger log = LoggerFactory.getLogger(POIManager.class);

	List<POIDetails> list = new ArrayList<POIDetails>();

	JSONObject jsonObject;
	public String ID;
	public String DisplayName;
	public String AddressLine1;
	public double Latitude;
	public double Longitude;
	
	public String place;
	public double longitude;
	public double latitude;
	public int radius;
	public String key;
	
	public String ADFPOI;

	@Async
	public Future<String> GooglePOIDetails() {
		log.debug("POIManager.GooglePOIDetails() called.");
		
		/*File file = new File("google_json_url_conf.xml");
		String absolutePath = file.getAbsolutePath();*/
		
		ApplicationContext context = new ClassPathXmlApplicationContext("google_json_url_conf.xml");

		URLProperties properties = (URLProperties) context
				.getBean("URLProperties");

		place = properties.getPlace();
		longitude = Double.parseDouble(properties.getLongitude());
		latitude = Double.parseDouble(properties.getLatitude());
		radius = Integer.parseInt(properties.getRadius());
		key = properties.getKey();

		String JSONUrl = "https://maps.googleapis.com/maps/api/place/textsearch/json?query="
				+ place
				+ "&location="
				+ longitude
				+ ","
				+ latitude
				+ "&radius=" + radius + "&key=" + key + "";

		try {
			/*Thread thread =  new Thread();
			thread.sleep(2000);*/
			jsonObject = new JSONObject(IOUtils.toString(new URL(JSONUrl),
					Charset.forName("UTF-8")));
			JSONArray jarray = (JSONArray) jsonObject.get("results");

			for (int i = 0; i < jarray.length(); i++) {
				JSONObject first = (JSONObject) jarray.get(i);

				ID = (String) first.get("place_id");
				DisplayName = (String) first.get("name");
				AddressLine1 = (String) first.get("formatted_address");
				JSONObject geometry = (JSONObject) first.get("geometry");
				JSONObject location = (JSONObject) geometry.get("location");
				Latitude = location.getDouble("lat");
				Longitude = location.getDouble("lng");

				list.add(new POIDetails(ID, DisplayName, AddressLine1,
						Latitude, Longitude));

				log.debug("Setting POI Details into the claim store");
			}

		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ADFPOI = "You have exceeded your daily request quota for this API";
		}

		ObjectMapper mapperObj = new ObjectMapper();
		

		try {
			ADFPOI = mapperObj.writeValueAsString(list);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println(ADFPOI);

		return new AsyncResult<String>(ADFPOI);
	}
}
