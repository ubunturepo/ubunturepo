package com.tridion.ambientdata.extensions.bean;

import org.codehaus.jackson.annotate.JsonProperty;

public class POIDetails {
	private String ID;
	private String DisplayName;
	private String AddressLine1;
	private double Latitude;
	private double Longitude;

	public POIDetails(String ID, String DisplayName, String AddressLine1,
			double Latitude, double Longitude) {
		// TODO Auto-generated constructor stub
		this.ID = ID;
		this.DisplayName = DisplayName;
		this.AddressLine1 = AddressLine1;
		this.Latitude = Latitude;
		this.Longitude = Longitude;
	}

	public POIDetails() {
		// TODO Auto-generated constructor stub
	}

	@JsonProperty("ID")
	public String getID() {
		return ID;
	}

	public void setID(String ID) {
		this.ID = ID;
	}

	@JsonProperty("DisplayName")
	public String getDisplayName() {
		return DisplayName;
	}

	public void setDisplayName(String DisplayName) {
		this.DisplayName = DisplayName;
	}

	@JsonProperty("AddressLine1")
	public String getAddressLine1() {
		return AddressLine1;
	}

	public void setAddressLine1(String AddressLine1) {
		this.AddressLine1 = AddressLine1;
	}

	@JsonProperty("Latitude")
	public double getLatitude() {
		return Latitude;
	}

	public void setLatitude(double Latitude) {
		this.Latitude = Latitude;
	}

	@JsonProperty("Longitude")
	public double getLongitude() {
		return Longitude;
	}

	public void setLongitude(double Longitude) {
		this.Longitude = Longitude;
	}

}
