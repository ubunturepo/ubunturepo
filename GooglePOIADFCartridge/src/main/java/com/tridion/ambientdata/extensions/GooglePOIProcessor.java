package com.tridion.ambientdata.extensions;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.tridion.ambientdata.AmbientDataException;
import com.tridion.ambientdata.claimstore.ClaimStore;
import com.tridion.ambientdata.processing.AbstractClaimProcessor;

public class GooglePOIProcessor extends AbstractClaimProcessor {

	private final Logger LOG = LoggerFactory
			.getLogger(GooglePOIProcessor.class);

	String jsonData, longitude, latitude;
	ApplicationContext context = new AnnotationConfigApplicationContext(
			POIConfig.class);
	POIManager manager = context.getBean(POIManager.class);

	Future<String> future = manager.GooglePOIDetails();

	public void onRequestStart(ClaimStore claims) throws AmbientDataException {
		LOG.debug("GooglePOIProcessor.onRequestStart() called.");

		try {
			longitude = claims.get(new URI("taf:claim:geolocation:longitude")).toString();
			latitude = claims.get(new URI("taf:claim:geolocation:latitude")).toString();
			jsonData = future.get();
		} catch (InterruptedException | ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			claims.put(new URI("taf:claim:context:nearestPOI:nearby"), jsonData);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onRequestEnd(ClaimStore claims) throws AmbientDataException {
		LOG.debug("GooglePOIProcessor.onRequestEnd() called.");

	}

	public void onSessionStart(ClaimStore claims) throws AmbientDataException {
		LOG.debug("GooglePOIProcessor.onSessionStart() called.");

	}

}
